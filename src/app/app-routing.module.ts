import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PostulanteFormComponent } from './postulante-form/postulante-form.component';

const routes: Routes = [
  { path: '', component: PostulanteFormComponent },
  { path: '**', pathMatch: 'full', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
