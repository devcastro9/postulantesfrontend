import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { RestDataService } from './services/rest-data.service';
import { PostulanteFormComponent } from './postulante-form/postulante-form.component';
import { OpfileService } from './services/opfile.service';

@NgModule({
  declarations: [
    AppComponent,
    PostulanteFormComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    NgbModule,
    SweetAlert2Module.forRoot()
  ],
  providers: [
    RestDataService,
    OpfileService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
