import { HttpEventType } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { delay } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import Swal from 'sweetalert2';
import { Beneficiario } from '../models/beneficiario.model';

import { Departamento } from '../models/departamento.model';
import { Educacion } from '../models/educacion.model';
import { EstadoCivil } from '../models/estadocivil.model';
import { Estudio } from '../models/estudio_realizado.model';
import { Experiencia } from '../models/experiencia_laboral.model';
import { Genero } from '../models/genero.model';
import { Habilidad } from '../models/habilidad.model';
import { Idioma } from '../models/idioma.model';
import { Pais } from '../models/pais.model';
import { Puesto } from '../models/puesto.model';
import { TipoDocumento } from '../models/tipodocumento.model';
import { UnidadMedida } from '../models/unidadmedida.model';
import { OpfileService } from '../services/opfile.service';

import { RestDataService } from '../services/rest-data.service';

@Component({
  selector: 'app-postulante-form',
  templateUrl: './postulante-form.component.html',
  styleUrls: ['./postulante-form.component.scss']
})
export class PostulanteFormComponent implements OnInit {
  // Formulario
  postulanteForm: FormGroup;
  // File
  process: boolean = false;
  inProgress: boolean = false;
  isCharged: boolean = false;
  progressPercent: number = 0;
  // Datos
  Departamentos: Departamento[] = [];
  NEducaciones: Educacion[] = [];
  EstadoCivil: EstadoCivil[] = [];
  Generos: Genero[] = [];
  Paises: Pais[] = [];
  Puestos: Puesto[] = [];
  TipoDocumentos: TipoDocumento[] = [];
  UnidadMedidas: UnidadMedida[] = [];

  constructor(private fb: FormBuilder, private data: RestDataService, private fileServ: OpfileService) {
    this.postulanteForm = this.fb.group({
      puestoId: [0, [Validators.required, Validators.min(1)]],
      documentoIdentidad: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(20)]],
      documentoRespaldoId: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(3)]],
      departamentoId: [0, [Validators.required, Validators.min(1)]],
      primerApellido: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(35)]],
      segundoApellido: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(35)]],
      nombres: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(50)]],
      fechaNacimiento: [new Date(), [Validators.required]],
      paisId: [31, [Validators.required, Validators.min(1)]],
      generoId: [0, [Validators.required, Validators.min(1)]],
      estadoCivilId: ['', [Validators.required]],
      telefonoFijo: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(30)]],
      telefonoCelular: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(30)]],
      emailPersonal: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(50), Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]],
      domicilioLegal: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(250)]],
      licenciaConducir: [false, [Validators.required]],
      licenciaMotocicleta: [false, [Validators.required]],
      rutaFoto: ['', [Validators.required, Validators.minLength(5)]],
      pretensionSalarial: [0, [Validators.required, Validators.min(1)]],
      estudiosRealizadosP: this.fb.array([], Validators.required),
      estudiosRealizadosS: this.fb.array([], Validators.required),
      estudiosRealizadosO: this.fb.array([], Validators.required),
      experienciasLaborales: this.fb.array([], Validators.required),
      habilidades: this.fb.array([], Validators.required),
      idiomas: this.fb.array([], Validators.required)
    });
  }

  ngOnInit(): void {
    // Datos
    this.data.getDepartamento().subscribe(
      (result): Departamento[] => this.Departamentos = result
    );
    this.data.getEducacion().subscribe(
      (result): Educacion[] => this.NEducaciones = result
    );
    this.data.getEstadoCivil().subscribe(
      (result): EstadoCivil[] => this.EstadoCivil = result
    );
    this.data.getGenero().subscribe(
      (result): Genero[] => this.Generos = result
    );
    this.data.getPuesto().subscribe(
      (result): Puesto[] => this.Puestos = result
    );
    this.data.getPaises('bol').subscribe(
      (result): Pais[] => this.Paises = result
    );
    this.data.getTipoDocumento().subscribe(
      (result): TipoDocumento[] => this.TipoDocumentos = result
    );
    this.data.getUnidadesMedida().subscribe(
      (result): UnidadMedida[] => this.UnidadMedidas = result
    );
  }

  get gEstudiosP() {
    return this.postulanteForm.get('estudiosRealizadosP') as FormArray;
  }

  get gEstudiosS() {
    return this.postulanteForm.get('estudiosRealizadosS') as FormArray;
  }

  get gEstudiosO() {
    return this.postulanteForm.get('estudiosRealizadosO') as FormArray;
  }

  get gExperiencias() {
    return this.postulanteForm.get('experienciasLaborales') as FormArray;
  }

  get gHabilidades() {
    return this.postulanteForm.get('habilidades') as FormArray;
  }

  get gIdiomas() {
    return this.postulanteForm.get('idiomas') as FormArray;
  }

  validador(nombre: string): boolean {
    return this.postulanteForm.get(nombre) !== null && this.postulanteForm.get(nombre)!.invalid && this.postulanteForm.get(nombre)!.touched;
  }

  addEducacion(tipoN: number): void {
    switch (tipoN) {
      case 1:
        this.gEstudiosP.push(this.fb.group({
          centroEducativo: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(80)]],
          tituloObtenido: [''],
          nivelEducativoId: [tipoN + 1],
          tipo: [tipoN],
          ciudad: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(50)]],
          paisId: [31, [Validators.required, Validators.min(1)]],
          fechaInicio: [new Date(), [Validators.required]],
          fechaFinalizacion: [new Date(), [Validators.required]],
          estudiandoActualmente: [false, [Validators.required]]
        }));
        break;
      case 2:
        this.gEstudiosS.push(this.fb.group({
          centroEducativo: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(80)]],
          tituloObtenido: [''],
          nivelEducativoId: [tipoN + 1],
          tipo: [tipoN],
          ciudad: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(50)]],
          paisId: [31, [Validators.required, Validators.min(1)]],
          fechaInicio: [new Date(), [Validators.required]],
          fechaFinalizacion: [new Date(), [Validators.required]],
          estudiandoActualmente: [false, [Validators.required]]
        }));
        break;
      default:
        this.gEstudiosO.push(this.fb.group({
          centroEducativo: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(80)]],
          tituloObtenido: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(80)]],
          nivelEducativoId: [0, [Validators.required, Validators.min(4)]],
          tipo: [tipoN],
          ciudad: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(50)]],
          paisId: [31, [Validators.required, Validators.min(1)]],
          fechaInicio: [new Date(), [Validators.required]],
          fechaFinalizacion: [new Date(), [Validators.required]],
          estudiandoActualmente: [false, [Validators.required]]
        }));
        break;
    }
    // this.gEstudiosP.push(this.fb.group({
    //   centroEducativo: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
    //   tituloObtenido: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
    //   nivelEducativoId: [0, [Validators.required, Validators.min(1)]],
    //   tipo: [tipoN],
    //   ciudad: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
    //   paisId: [0, [Validators.required, Validators.min(1)]],
    //   fechaInicio: [new Date(), [Validators.required]],
    //   fechaFinalizacion: [new Date(), [Validators.required]],
    //   estudiandoActualmente: [false, [Validators.required]]
    // }));
  }

  addExperiencia(): void {
    this.gExperiencias.push(this.fb.group({
      nombreInstitucion: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(50)]],
      cargo: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(50)]],
      funcionGeneral: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(500)]],
      paisId: [31, [Validators.required, Validators.min(1)]],
      ciudad: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(50)]],
      fechaInicio: [new Date(), [Validators.required]],
      fechaFinalizacion: [new Date(), [Validators.required]]
    }));
  }

  addHabilidad(): void {
    this.gHabilidades.push(this.fb.group({
      descripcion: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(150)]],
      tiempo: [0, [Validators.required, Validators.min(0)]],
      unidadMedidaId: [0, [Validators.required, Validators.min(1)]]
    }));
  }

  addIdioma(): void {
    this.gIdiomas.push(this.fb.group({
      idiomaDescripcion: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(100)]],
      nivelEscrito: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(150)]],
      nivelOral: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(150)]],
      nivelLectura: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(150)]]
    }));
  }

  delEducacion(index: number, tipo: number): void {
    switch (tipo) {
      case 1:
        this.gEstudiosP.removeAt(index);
        break;
      case 2:
        this.gEstudiosS.removeAt(index);
        break;
      default:
        this.gEstudiosO.removeAt(index);
        break;
    }
  }

  delExperiencia(index: number): void {
    this.gExperiencias.removeAt(index);
  }

  delHabilidad(index: number): void {
    this.gHabilidades.removeAt(index);
  }

  delIdioma(index: number): void {
    this.gIdiomas.removeAt(index);
  }

  selectFile(event: Event): void {
    const archivos = (<HTMLInputElement>event.target).files;
    if (archivos === null || archivos.length === 0) {
      return;
    }
    const foto = archivos[0];
    this.fileServ.uploadFile(foto).subscribe(
      (result): void => {
        if (result) {
          switch (result.type) {
            case HttpEventType.Sent:
              this.process = true;
              this.isCharged = false;
              this.inProgress = true;
              this.progressPercent = 0;
              break;
            case HttpEventType.UploadProgress:
              this.isCharged = false;
              this.inProgress = true;
              this.progressPercent = (result.loaded / result.total!) * 100;
              break;
            case HttpEventType.DownloadProgress:
              this.isCharged = false;
              this.inProgress = true;
              this.progressPercent = (result.loaded / result.total!) * 100;
              break;
            case HttpEventType.Response:
              this.process = false;
              this.isCharged = true;
              this.inProgress = false;
              this.progressPercent = 100;
              if (result.body?.estado) {
                this.postulanteForm.get('rutaFoto')?.setValue(result.body?.ruta === undefined ? '' : result.body?.ruta);
              } else {
                this.postulanteForm.get('rutaFoto')?.setValue('-');
              }
              break;
            case HttpEventType.User:
              this.process = false;
              this.isCharged = false;
              this.inProgress = false;
              this.postulanteForm.get('rutaFoto')?.setValue('-1');
              break;
            default:
              this.process = false;
              this.isCharged = false;
              this.inProgress = false;
              this.postulanteForm.get('rutaFoto')?.setValue('0');
              break;
          }
        }
      }
    );
  }

  Guardar(): void {
    // Si el formulario es valido
    if (this.postulanteForm.valid) {
      let postulante: Beneficiario = {
        puestoId: this.postulanteForm.value.puestoId,
        documentoIdentidad: this.postulanteForm.value.documentoIdentidad,
        documentoRespaldoId: this.postulanteForm.value.documentoRespaldoId,
        departamentoId: this.postulanteForm.value.departamentoId,
        primerApellido: this.postulanteForm.value.primerApellido,
        segundoApellido: this.postulanteForm.value.segundoApellido,
        nombres: this.postulanteForm.value.nombres,
        fechaNacimiento: this.postulanteForm.value.fechaNacimiento,
        paisId: this.postulanteForm.value.paisId,
        generoId: this.postulanteForm.value.generoId,
        estadoCivilId: this.postulanteForm.value.estadoCivilId,
        telefonoFijo: this.postulanteForm.value.telefonoFijo,
        telefonoCelular: this.postulanteForm.value.telefonoCelular,
        emailPersonal: this.postulanteForm.value.emailPersonal,
        domicilioLegal: this.postulanteForm.value.domicilioLegal,
        licenciaConducir: this.postulanteForm.value.licenciaConducir,
        licenciaMotocicleta: this.postulanteForm.value.licenciaMotocicleta,
        fotoRuta: this.postulanteForm.value.rutaFoto,
        pretensionSalarial: this.postulanteForm.value.pretensionSalarial,
        estudiosRealizados: [],
        experienciaLaborales: [],
        habilidades: [],
        idiomas: []
      };
      this.postulanteForm.value.estudiosRealizadosP.forEach((element: Estudio) => {
        postulante.estudiosRealizados.push(element);
      });
      this.postulanteForm.value.estudiosRealizadosS.forEach((element: Estudio) => {
        postulante.estudiosRealizados.push(element);
      });
      this.postulanteForm.value.estudiosRealizadosO.forEach((element: Estudio) => {
        postulante.estudiosRealizados.push(element);
      });
      this.postulanteForm.value.experienciasLaborales.forEach((element: Experiencia) => {
        postulante.experienciaLaborales.push(element);
      });
      this.postulanteForm.value.habilidades.forEach((element: Habilidad) => {
        postulante.habilidades.push(element);
      });
      this.postulanteForm.value.idiomas.forEach((element: Idioma) => {
        postulante.idiomas.push(element);
      });
      // Ver resultado
      if (!environment.production) {
        console.log(postulante);
      }
      // POST
      this.data.addPostulante(postulante).subscribe(
        (result): void => {
          if (!result.Error) {
            Swal.fire('Postulacion guardada', 'Se lo redirigira a la pagina de inicio en 3 segundos', 'success');
            setTimeout(() => {
              window.location.href = "https://otisbolivia.com/index.php/empresa/trabaja-con-nosotros";
            }, 3000);
          } else {
            Swal.fire('Ha ocurrido un error', result.Mensaje, 'error');
          }
        }
      );
    } else {
      const toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
          toast.addEventListener('mouseenter', Swal.stopTimer)
          toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
      });
      toast.fire({
        icon: 'error',
        title: 'Complete todos los campos'
      });
    }
  }
}
