import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PostulanteFormComponent } from './postulante-form.component';

describe('PostulanteFormComponent', () => {
  let component: PostulanteFormComponent;
  let fixture: ComponentFixture<PostulanteFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PostulanteFormComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PostulanteFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
