import { FormArray, FormControl } from "@angular/forms";

export interface PostulanteForm {
    puestoId: FormControl<number>;
    documentoIdentidad: FormControl<string>;
    documentoRespaldo: FormControl<string>;
    departamentoId: FormControl<number>;
    primerApellido: FormControl<string>;
    segundoApellido: FormControl<string>;
    nombres: FormControl<string>;
    fechaNacimiento: FormControl<Date>;
    paisId: FormControl<number>;
    generoId: FormControl<number>;
    estadoCivilId: FormControl<string>;
    telefonoFijo: FormControl<string>;
    telefonoCelular: FormControl<string>;
    emailPersonal: FormControl<string>;
    domiciliioLegal: FormControl<string>;
    licenciaConducir: FormControl<boolean>;
    licenciaMotocicleta: FormControl<boolean>;
    fotoRuta: FormControl<string>;
    pretensionSalarial: FormControl<number>;
    // estudiosForm: FormArray[];
    // experienciaForm: FormArray[];
    // habilidadForm: FormArray[];
    // idiomaForm: FormArray[];
}
