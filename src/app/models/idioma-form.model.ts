import { FormControl } from "@angular/forms";

export interface Idioma {
    idiomaDescripcion: FormControl<string>;
    nivelEscrito: FormControl<string>;
    nivelOral: FormControl<string>;
    nivelLectura: FormControl<string>;
}