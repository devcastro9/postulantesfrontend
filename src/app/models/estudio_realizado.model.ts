export interface Estudio {
    centroEducativo: string;
    tituloObtenido: string | null;
    tipo: number;
    ciudad: string;
    paisId: number;
    fechaInicio: Date;
    fechaFinalizacion: Date;
    estudiandoActualmente: boolean;
}