import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';

import { Beneficiario } from '../models/beneficiario.model';
import { Departamento } from '../models/departamento.model';
import { Educacion } from '../models/educacion.model';
import { EstadoCivil } from '../models/estadocivil.model';
import { Genero } from '../models/genero.model';
import { Puesto } from '../models/puesto.model';
import { TipoDocumento } from '../models/tipodocumento.model';
import { Pais } from '../models/pais.model';
import { UnidadMedida } from '../models/unidadmedida.model';
import { environment } from 'src/environments/environment';
import { Respuesta } from '../models/respuesta.model';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class RestDataService {

  constructor(private http: HttpClient) { }

  addPostulante(postulante: Beneficiario): Observable<Respuesta> {
    return this.http.post<Respuesta>(environment.apiUrl + 'Beneficiarios', postulante, httpOptions);
  }

  getDepartamento(): Observable<Departamento[]> {
    return this.http.get<Departamento[]>(environment.apiUrl + 'Departamentos');
  }

  getEducacion(): Observable<Educacion[]> {
    return this.http.get<Educacion[]>(environment.apiUrl + "RcNivelEducacionales");
  }

  getEstadoCivil(): Observable<EstadoCivil[]> {
    return this.http.get<EstadoCivil[]>(environment.apiUrl + "RcEstadoCiviles");
  }

  getGenero(): Observable<Genero[]> {
    return this.http.get<Genero[]>(environment.apiUrl + "Generos");
  }

  getPaises(filter: string): Observable<Pais[]> {
    return this.http.get<Pais[]>(environment.apiUrl + "GcPaises/" + filter);
  }

  getPuesto(): Observable<Puesto[]> {
    return this.http.get<Puesto[]>(environment.apiUrl + "Puestos");
  }

  getTipoDocumento(): Observable<TipoDocumento[]> {
    return this.http.get<TipoDocumento[]>(environment.apiUrl + "GcTipoDocumentoIds");
  }

  getUnidadesMedida(): Observable<UnidadMedida[]> {
    return this.http.get<UnidadMedida[]>(environment.apiUrl + 'UnidadMedidaTiempos');
  }
}
